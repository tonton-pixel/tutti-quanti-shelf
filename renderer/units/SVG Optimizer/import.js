//
const unit = document.getElementById ('svg-optimizer-unit');
//
const clearButton = unit.querySelector ('.clear-button');
const samplesButton = unit.querySelector ('.samples-button');
const loadButton = unit.querySelector ('.load-button');
const saveButton = unit.querySelector ('.save-button');
const inputString = unit.querySelector ('.input-string');
const percentage = unit.querySelector ('.percentage');
const outputSaveButton = unit.querySelector ('.output-save-button');
const outputString = unit.querySelector ('.output-string');
//
const references = unit.querySelector ('.references');
const links = unit.querySelector ('.links');
//
let defaultFolderPath;
//
module.exports.start = function (context)
{
    const fs = require ('fs');
    const path = require ('path');
    //
    const fileDialogs = require ('../../lib/file-dialogs.js');
    const pullDownMenus = require ('../../lib/pull-down-menus.js');
    const sampleMenus = require ('../../lib/sample-menus');
    //
    const { optimize } = require ('svgo');
    //
    const defaultPrefs =
    {
        inputString: "",
        defaultFolderPath: context.defaultFolderPath,
        references: false
    };
    let prefs = context.getPrefs (defaultPrefs);
    //
    clearButton.addEventListener
    (
        'click',
        (event) =>
        {
            inputString.value = "";
            inputString.dispatchEvent (new Event ('input'));
            inputString.focus ();
        }
    );
    //
    let samplesDirname = path.join (__dirname, 'samples');
    let samplesFilenames = fs.readdirSync (samplesDirname);
    samplesFilenames.sort ((a, b) => a.localeCompare (b));
    let samples = [ ];
    for (let samplesFilename of samplesFilenames)
    {
        let filename = path.join (samplesDirname, samplesFilename);
        if (fs.statSync (filename).isDirectory ())
        {
            let dirname = filename;
            let itemsFilenames = fs.readdirSync (dirname);
            itemsFilenames.sort ((a, b) => a.localeCompare (b));
            let items = [ ];
            for (let itemsFilename of itemsFilenames)
            {
                let filename = path.join (dirname, itemsFilename);
                if (fs.statSync (filename).isFile ())
                {
                    let jsFilename = itemsFilename.match (/(.*)\.svg$/i);
                    if (jsFilename)
                    {
                        items.push ({ label: jsFilename[1], string: fs.readFileSync (filename, 'utf8') });
                    }
                }
            }
            samples.push ({ label: samplesFilename, items: items });
        }
        else if (fs.statSync (filename).isFile ())
        {
            let jsFilename = samplesFilename.match (/(.*)\.svg$/i);
            if (jsFilename)
            {
                samples.push ({ label: jsFilename[1], string: fs.readFileSync (filename, 'utf8') });
            }
        }
    }
    //
    let samplesMenu = sampleMenus.makeMenu
    (
        samples,
        (sample) =>
        {
            inputString.value = sample.string;
            inputString.scrollTop = 0;
            inputString.scrollLeft = 0;
            inputString.dispatchEvent (new Event ('input'));
        }
    );
    //
    samplesButton.addEventListener
    (
        'click',
        (event) =>
        {
            pullDownMenus.popup (event.currentTarget, samplesMenu);
        }
    );
    //
    defaultFolderPath = prefs.defaultFolderPath;
    //
    function performOptimization (svg)
    {
        let error = false;
        let errorMessage = "";
        let output = "";
        if (svg)
        {
            let result = optimize
            (
                svg,
                {
                    multipass: true,
                    plugins:
                    [
                        "convertPathData",
                        "convertTransform",
                        "cleanupNumericValues",
                        "mergePaths",
                        "convertShapeToPath",
                        "convertEllipseToCircle",
                        //
                        "removeXMLProcInst",
                        "removeDoctype",
                        "removeComments",
                        "removeMetadata",
                        "removeEditorsNSData",
                        //
                        // "removeTitle",
                        // "cleanupAttrs",
                        // "cleanupIDs"
                    ]
                }
            );
            if ('data' in result)
            {
                output = result.data.replaceAll ('\n', '&#10;').replaceAll ('\r', '&#13;'); // For multiline tooltips;
            }
            else
            {
                error = true;
                errorMessage = "[svgo] " + result.modernError.message;
            }
        }
        outputString.scrollTop = 0;
        outputString.scrollLeft = 0;
        if (error)
        {
            outputString.value = errorMessage;
            percentage.textContent = "";
            outputString.classList.add ('output-error');
            outputSaveButton.disabled = true;
        }
        else
        {
            outputString.value = output;
            let inputLength = inputString.value.length;
            let outputLength = outputString.value.length;
            percentage.textContent = outputLength ? (((outputLength / inputLength) - 1) * 100).toFixed (2) + "%" : "";
            outputString.classList.remove ('output-error');
            // outputSaveButton.disabled = (outputLength === 0);
            outputSaveButton.disabled = false;  // Consistent with saveButton
        }
    }
    //
    inputString.addEventListener ('input', event => { performOptimization (event.currentTarget.value.trim ()) });
    inputString.value = prefs.inputString;
    inputString.dispatchEvent (new Event ('input'));
    //
    const loadXmlFileFilters =
    [
        { name: "SVG File (*.svg)", extensions: [ 'svg' ] }
    ];
    //
    loadButton.addEventListener
    (
        'click',
        (event) =>
        {
            fileDialogs.loadTextFile
            (
                "Load SVG file:",
                loadXmlFileFilters,
                defaultFolderPath,
                'utf8',
                (text, filePath) =>
                {
                    inputString.value = text;
                    inputString.scrollTop = 0;
                    inputString.scrollLeft = 0;
                    inputString.dispatchEvent (new Event ('input'));
                    defaultFolderPath = path.dirname (filePath);
                }
            );
        }
    );
    //
    const saveSvgFileFilters =
    [
        { name: "SVG File (*.svg)", extensions: [ 'svg' ] }
    ];
    //
    saveButton.addEventListener
    (
        'click',
        (event) =>
        {
            fileDialogs.saveTextFile
            (
                "Save SVG file:",
                saveSvgFileFilters,
                defaultFolderPath,
                (filePath) =>
                {
                    defaultFolderPath = path.dirname (filePath);
                    return inputString.value;
                }
            );
        }
    );
    //
    const svgFileFilters =
    [
        { name: "SVG File (*.svg)", extensions: [ 'svg' ] }
    ];
    //
    outputSaveButton.addEventListener
    (
        'click',
        (event) =>
        {
            fileDialogs.saveTextFile
            (
                "Save SVG file:",
                svgFileFilters,
                defaultFolderPath,
                (filePath) =>
                {
                    defaultFolderPath = path.dirname (filePath);
                    return outputString.value;
                }
            );
        }
    );
    //
    references.open = prefs.references;
    //
    const refLinks = require ('./ref-links.json');
    const linksList = require ('../../lib/links-list.js');
    //
    linksList (links, refLinks);
};
//
module.exports.stop = function (context)
{
    let prefs =
    {
        inputString: inputString.value,
        defaultFolderPath: defaultFolderPath,
        references: references.open
    };
    context.setPrefs (prefs);
};
//
